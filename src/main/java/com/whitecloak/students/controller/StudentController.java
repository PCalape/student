package com.whitecloak.students.controller;

import com.whitecloak.students.model.StudentEntity;
import com.whitecloak.students.model.StudentRequest;
import com.whitecloak.students.model.StudentResponse;
import com.whitecloak.students.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/students")
public class StudentController {

    private StudentService studentService;

    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @PostMapping
    public StudentResponse register(@RequestBody StudentRequest newUser) {
        StudentEntity student = studentService.register(newUser);
        return new StudentResponse(
                student.getFirstName(),
                student.getLastName(),
                student.getUsername(),
                student.getEmail(),
                student.getAddress(),
                student.getPhone());
    }

    @GetMapping
    public List<StudentResponse> getUsers() {
        List<StudentEntity> student = studentService.getUsers();
        List<StudentResponse> output = new ArrayList<>();
        student.forEach(a ->
                output.add(new StudentResponse(
                a.getFirstName(),
                a.getLastName(),
                a.getUsername(),
                a.getEmail(),
                a.getAddress(),
                a.getPhone())));
        return output;
    }

    @GetMapping("/{id}")
    public StudentResponse getOneUser(@PathVariable("id") Long id) {
        StudentEntity student = studentService.getOneUser(id).get();
        return new StudentResponse(
                student.getFirstName(),
                student.getLastName(),
                student.getUsername(),
                student.getEmail(),
                student.getAddress(),
                student.getPhone());
    }

    @PutMapping("/{id}")
    public void changeUserDetails(@PathVariable("id") Long id,
                                           @RequestBody StudentRequest editedUser) {
        studentService.changeUserDetails(id, editedUser);
    }

    @PatchMapping("/{id}")
    public void changeUserDetails2(@PathVariable("id") Long id,
                                  @RequestBody StudentRequest editedUser) {
        studentService.changeUserDetails2(id, editedUser);
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable("id") Long id) {
        studentService.deleteUser(id);
    }

}
