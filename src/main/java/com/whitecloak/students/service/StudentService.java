package com.whitecloak.students.service;

import com.whitecloak.students.model.StudentEntity;
import com.whitecloak.students.model.StudentRequest;
import com.whitecloak.students.model.StudentResponse;

import java.util.List;
import java.util.Optional;

public interface StudentService {
    StudentEntity register(StudentRequest newUser);
    List<StudentEntity> getUsers();
    Optional<StudentEntity> getOneUser(Long id);
    void changeUserDetails(Long id, StudentRequest editedUser);
    void changeUserDetails2(Long id, StudentRequest editedUser);
    void deleteUser(Long id);
}
