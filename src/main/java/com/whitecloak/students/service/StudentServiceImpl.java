package com.whitecloak.students.service;

import com.whitecloak.students.model.StudentEntity;
import com.whitecloak.students.model.StudentRequest;
import com.whitecloak.students.model.StudentResponse;
import com.whitecloak.students.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@Service
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;

    @Autowired
    public StudentServiceImpl(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Override
    public StudentEntity register(StudentRequest newUser) {
        StudentEntity newUserForSaving = new StudentEntity(
                newUser.getFirstName(),
                newUser.getLastName(),
                newUser.getUsername(),
                newUser.getEmail(),
                newUser.getAddress(),
                newUser.getPhone());
        return studentRepository.save(newUserForSaving);
    }

    @Override
    public List<StudentEntity> getUsers() {
        return studentRepository.findAll();
    }

    @Override
    public Optional<StudentEntity> getOneUser(Long id) {
        return studentRepository.findById(id);
    }

    @Override
    public void changeUserDetails(Long id, StudentRequest editedUser) {
        Optional<StudentEntity> forEditUser = studentRepository.findById(id);
        if (forEditUser.isEmpty()) throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        StudentEntity forEditUserUnwrapped = forEditUser.get();
        if (editedUser.getFirstName() != null) forEditUserUnwrapped.setFirstName(editedUser.getFirstName());
        if (editedUser.getLastName() != null) forEditUserUnwrapped.setLastName(editedUser.getFirstName());
        if (editedUser.getUsername() != null) forEditUserUnwrapped.setUsername(editedUser.getFirstName());
        if (editedUser.getAddress() != null) forEditUserUnwrapped.setAddress(editedUser.getFirstName());
        if (editedUser.getEmail() != null) forEditUserUnwrapped.setEmail(editedUser.getFirstName());
        if (editedUser.getPhone() != null) forEditUserUnwrapped.setPhone(editedUser.getFirstName());
        studentRepository.save(forEditUserUnwrapped);
    }

    @Override
    public void changeUserDetails2(Long id, StudentRequest editedUser) {
        Optional<StudentEntity> forEditUser = studentRepository.findById(id);
        if (forEditUser.isEmpty()) throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        StudentEntity forEditUserUnwrapped = forEditUser.get();
        if (editedUser.getFirstName() != null) forEditUserUnwrapped.setFirstName(editedUser.getFirstName());
        if (editedUser.getLastName() != null) forEditUserUnwrapped.setLastName(editedUser.getFirstName());
        if (editedUser.getUsername() != null) forEditUserUnwrapped.setUsername(editedUser.getFirstName());
        if (editedUser.getAddress() != null) forEditUserUnwrapped.setAddress(editedUser.getFirstName());
        if (editedUser.getEmail() != null) forEditUserUnwrapped.setEmail(editedUser.getFirstName());
        if (editedUser.getPhone() != null) forEditUserUnwrapped.setPhone(editedUser.getFirstName());
        studentRepository.save(forEditUserUnwrapped);
    }

    @Override
    public void deleteUser(Long id) {
        studentRepository.deleteById(id);
    }
}
