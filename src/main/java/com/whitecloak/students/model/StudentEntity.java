package com.whitecloak.students.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name = "students")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudentEntity {

    @Id
    @GeneratedValue(generator = "UUID")
    private UUID id;

    @JsonProperty("first_name")
    @Column(name = "first_name")
    private String firstName;

    @JsonProperty("last_name")
    @Column(name = "last_name")
    private String lastName;

    @JsonProperty("username")
    @Column(name = "username")
    private String username;

    @JsonProperty("email")
    @Column(name = "email")
    private String email;

    @JsonProperty("address")
    @Column(name = "address")
    private String address;

    @JsonProperty("phone")
    @Column(name = "phone")
    private String phone;

    public StudentEntity(String firstName, String lastName, String username, String email, String address, String phone) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.email = email;
        this.address = address;
        this.phone = phone;
    }
}
